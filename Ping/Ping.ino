/*
  Ping Example

 This example sends an ICMP pings every 500 milliseconds, sends the human-readable
 result over the serial port.

 Circuit:
 * Ethernet shield attached to pins 10, 11, 12, 13

 created 30 Sep 2010
 by Blake Foster

 */

#include <stdio.h>
#include <SPI.h>
#include <Ethernet.h>
#include "ICMPPing.h"

const char* getStatusStr(Status status);
void delayMin(unsigned long timeMin);

void ligar(void);
void desligar(void);

byte mac[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED}; // max address for ethernet shield
byte ip[] = {192, 168, 25, 177}; // ip address for ethernet shield
IPAddress pingAddrGoogle(8, 8, 8, 8); // ip address to ping
IPAddress pingAddrGVT(192, 168, 25, 1); // ip address to ping

SOCKET pingSocket = 0;

char buffer [256];
ICMPPing ping(pingSocket, (uint16_t)random(0, 255));

int relayPin1 = 7;
int relayPin2 = 6;

int seqRetries = 0;

void setup()
{
  Serial.begin(9600);
  Serial.println("===== Monitor de Internet =====");

  Serial.println("Configurando pinos (usando pinos 7 e 6)...");
  pinMode(relayPin1, OUTPUT);     //Configura pino do relé
  pinMode(relayPin2, OUTPUT);     //Configura pino do relé
  ligarModem();
  
  // start Ethernet
  Serial.println("Iniciando Ethernet com IP fixo 192.168.25.177...");
  Ethernet.begin(mac, ip);
  
  Serial.println("Rodando...");
  Serial.println("");
}

void loop()
{
  IPAddress& pingAddrTarget = pingAddrGoogle;
  ICMPEchoReply echoReply = ping(pingAddrTarget, 4);
  if (echoReply.status == SUCCESS)
  {
    printEchoReplySuccess(echoReply);

    seqRetries = 0;       //Zera contador
    delay(10 * 1000);     //Espera 10 segundos (manda um ping a cada 10 segundos, sempre que receber a resposta)
  }
  else
  {
    printEchoReplyError(echoReply, pingAddrTarget);
    
    if (++seqRetries < 20) 
    {
      delay(500);                     //Não houve resposta do ping, espera meio segundo e tenta novamente

    }else{
      resetarModem();
      
      seqRetries = 0; 			//Zera contador
    }
  }

  delay(10);
}



const char* getStatusStr(Status status) {

  if (status == SUCCESS)
    return "SUCCESS";
  else if (status == SEND_TIMEOUT)
    return "SEND_TIMEOUT";
  else if (status == NO_RESPONSE)
    return "NO_RESPONSE";
  else if (status == BAD_RESPONSE)
    return "BAD_RESPONSE";
  else
    return "INVALID ICMP RESPONSE";
}


void delayMin(unsigned long timeMin){
  while(timeMin-- > 0){
    delay(1000); //Delay de 1 segundo
  }
}

void ligarModem(void){
  digitalWrite(relayPin1, LOW);
  digitalWrite(relayPin2, LOW);
}

void desligarModem(void){
  digitalWrite(relayPin1, HIGH);
  digitalWrite(relayPin2, HIGH);
}

void resetarModem(void){
  //Excedeu a quantidade máxima de timeouts do ping
  Serial.println("Excedeu a quantidade máxima de timeouts do ping");
  
  Serial.println("Desligando modem...");
  desligarModem();
  delay(5000);			// Espera 5 segundos
	  
  Serial.println("Ligando modem...");
  ligarModem();

  Serial.println("Esperando dois minutos, até que o modem se reconecte a internet...");
  delayMin(2);                 //Espera 2 minutos
}

void printEchoReplySuccess(ICMPEchoReply & echoReply){
      snprintf(buffer, sizeof(buffer),
             "Reply[%d] from: %d.%d.%d.%d: bytes=%d time=%ldms TTL=%d",
             echoReply.data.seq,
             echoReply.addr[0],
             echoReply.addr[1],
             echoReply.addr[2],
             echoReply.addr[3],
             REQ_DATASIZE,
             millis() - echoReply.data.time,
             echoReply.ttl);
    Serial.println(buffer);
}

void printEchoReplyError(ICMPEchoReply & echoReply, const IPAddress& pingAddr){
      snprintf(buffer, sizeof(buffer), "Echo request failed from %d.%d.%d.%d: %s, retries count: %d", 
            pingAddr[0],
            pingAddr[1],
            pingAddr[2],
            pingAddr[3],
            getStatusStr(echoReply.status), 
            seqRetries);
    Serial.println(buffer);
}




