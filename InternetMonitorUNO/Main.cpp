/*
 * Main.cpp
 *
 *  Created on: 06/06/2015
 *      Author: David Alain
 */

#include <Arduino.h>
#include "InternetMonitor.h"

byte deviceMAC[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED}; // MAC address for Ethernet shield
IPAddress deviceAddress(192, 168, 25, 177); // IP address for Ethernet shield
IPAddress gwAddress(192, 168, 25, 1); // IP address: DNS and Gateway
IPAddress googleAddress(8, 8, 8, 8); // IP address to ping

#define RELAY1_PIN 7
#define RELAY2_PIN 6

InternetMonitor internetMonitor(RELAY1_PIN, RELAY2_PIN);

void setup(){
	Serial.begin(9600);

	Serial.println("Iniciando Ethernet...");
	Ethernet.begin(deviceMAC, deviceAddress, gwAddress, gwAddress);

	Serial.println("Iniciando Monitor de Internet...");
	internetMonitor.begin(googleAddress);

	Serial.print("Rodando com IP ");
	Serial.println(InternetMonitor::addrToStr(deviceAddress));

	Serial.print("Testando contra o IP ");
	Serial.println(InternetMonitor::addrToStr(googleAddress));
	Serial.println();
}

void loop(){
	internetMonitor.check();
}



