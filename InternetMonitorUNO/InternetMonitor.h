#ifndef _InternetMonitor_H_
#define _InternetMonitor_H_

#include <Arduino.h>
#include <IPAddress.h>
#include <Ethernet.h>
#include "ICMPPing.h"

class InternetMonitor{

public:
	InternetMonitor(const int relay1Pin, const int relay2Pin);
	void begin(IPAddress& targetAddress);
	void check(void);
	static const char* addrToStr(IPAddress& addr);

private:
	IPAddress _targetAddress;

	int _relay1Pin;
	int _relay2Pin;

	int _seqRetries;
	char _print_buffer[256];

	inline void modemPowerOn(void);
	inline void modemPowerOff(void);
	void modemReset(void);
	void printEchoReply(ICMPEchoReply& echoReply);

};


#endif /* _InternetMonitor_H_ */
