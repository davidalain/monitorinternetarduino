/*
 * DelayWithBlink.h
 *
 *  Created on: 12/01/2016
 *      Author: David Alain
 */

#ifndef DELAYWITHBLINK_H_
#define DELAYWITHBLINK_H_

#include <stdint.h>	//for uint32_t

class DelayWithBlink {
public:

	DelayWithBlink(int ledPin);
	void microseconds(uint32_t msec);
	void seconds(uint32_t sec);
	void minutes(uint32_t min);

private:
	int _ledPin;
};

#endif /* DELAYWITHBLINK_H_ */
