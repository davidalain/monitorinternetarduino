/*
 * Main.cpp
 *
 *  Created on: 06/06/2015
 *      Author: David Alain
 */

#include <Arduino.h>
#include "InternetMonitor.h"

#define MODEM_GVT

byte deviceMAC[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED}; // MAC address for Ethernet shield
IPAddress googleAddress(8, 8, 8, 8); // IP address to ping

#ifdef MODEM_GVT
IPAddress deviceAddress(192, 168, 25, 177); // IP address for Ethernet shield
IPAddress gwAddress(192, 168, 25, 1); // IP address: DNS and Gateway
#else
IPAddress deviceAddress(192, 168, 0, 177); // IP address for Ethernet shield
IPAddress gwAddress(192, 168, 0, 1); // IP address: DNS and Gateway
#endif


#define RELAY1_PIN 		7
#define RELAY2_PIN 		6
#define LED_BLINK_PIN	13

InternetMonitor internetMonitor(RELAY1_PIN, RELAY2_PIN, LED_BLINK_PIN);

void setup(){

	Serial.begin(9600);

	Serial.print("Iniciando Ethernet... ");
	Ethernet.begin(deviceMAC, deviceAddress, gwAddress, gwAddress);
	Serial.println("iniciado!");

	Serial.print("IP: ");
	deviceAddress.printTo(Serial);
	Serial.println();

	Serial.print("Iniciando Monitor de Internet... ");
	internetMonitor.begin(googleAddress);
	Serial.println("iniciado!");

	Serial.print("Testando ping contra ");
	googleAddress.printTo(Serial);
	Serial.println("\r\n");


}

void loop(){
	internetMonitor.check();
}



