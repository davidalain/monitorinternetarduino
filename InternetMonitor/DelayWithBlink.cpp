/*
 * DelayWithBlink.cpp
 *
 *  Created on: 12/01/2016
 *      Author: David Alain
 */

#include "DelayWithBlink.h"
#include <Arduino.h>			//for delay() and digitalWrite

DelayWithBlink::DelayWithBlink(int ledPin):
_ledPin(ledPin)
{
	pinMode(_ledPin, OUTPUT);
}

void DelayWithBlink::microseconds(uint32_t msec){
	delay(msec);
}

void DelayWithBlink::seconds(uint32_t sec){
	while(sec-- > 0){
		delay(499);
		digitalWrite(_ledPin, digitalRead(_ledPin) ^ 1); //Toggle ledPin
		delay(499);
		digitalWrite(_ledPin, digitalRead(_ledPin) ^ 1); //Toggle ledPin
	}
}

void DelayWithBlink::minutes(uint32_t min){
	while(min-- > 0){
		this->seconds(60);
	}
}
