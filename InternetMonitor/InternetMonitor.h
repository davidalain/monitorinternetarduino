#ifndef _InternetMonitor_H_
#define _InternetMonitor_H_

#include <Arduino.h>
#include <IPAddress.h>
#include <Ethernet.h>
#include "ICMPPing.h"
#include "DelayWithBlink.h"

class InternetMonitor{

public:
	/**
	 * Construtor
	 *
	 * @param relay1Pin		Pino que estar� ligado ao rel� 1 (fio fase do roteador).
	 * @param relay2Pin		Pino que estar� ligado ao rel� 2 (fio neutro do roteador).
	 * @param ledBlinkPin	Pino com LED que ficar� piscando a cada 1 segundo durante o funcionamento para informar que o sistema est� funcionando.
	 */
	InternetMonitor(const int relay1Pin, const int relay2Pin, const int ledBlinkPin);

	/**
	 * Configura o endere�o IP alvo o qual ser� testado o ping
	 *
	 * @param targetAddress	Endere�o IP alvo
	 */
	void begin(IPAddress& targetAddress);

	/**
	 * Executa o teste de conex�o com a internet e reseta o modem caso n�o tenha conex�o.
	 */
	void check(void);

private:
	IPAddress _targetAddress;	//!< Endere�o IP alvo para teste da conex�o atrav�s de ping

	DelayWithBlink _delayBlink;	//!< Classe util para delay e blink
	int _relay1Pin;				//!< Pino ligado ao rel� 1
	int _relay2Pin;				//!< Pino ligado ao rel� 2

	int _seqRetries;			//!< Quantidade de tentativas de ping sem sucesso
	char _print_buffer[256];	//!< Buffer para composi��o das mensagens a serem escritas no console

	/**
	 * Liga o modem atrav�s dos rel�s
	 */
	inline void modemPowerOn(void);

	/**
	 * Desliga o modem atrav�s dos rel�s
	 */
	inline void modemPowerOff(void);

	/**
	 * Reseta o modem atrav�s dos rel�s
	 */
	void modemReset(void);

	/**
	 * Imprime no console serial uma mensagem da resposta do ping
	 *
	 * @param echoReply Resposta do ping
	 */
	void printEchoReply(ICMPEchoReply& echoReply);

};


#endif /* _InternetMonitor_H_ */
