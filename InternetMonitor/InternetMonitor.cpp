
#include <stdio.h>
#include <string.h>
#include <Arduino.h>
#include "InternetMonitor.h"
#include "DelayWithBlink.h"

static SOCKET pingSocket = 0;

static const char* statusStr(Status status) {

	if (status == SUCCESS) 		return "SUCCESS";
	if (status == SEND_TIMEOUT)	return "SEND_TIMEOUT";
	if (status == NO_RESPONSE)	return "NO_RESPONSE";
	if (status == BAD_RESPONSE)	return "BAD_RESPONSE";
	return "INVALID ICMP RESPONSE";
}

InternetMonitor::InternetMonitor(const int relay1Pin, const int relay2Pin, const int ledBlinkPin) :
	_delayBlink(ledBlinkPin), _relay1Pin(relay1Pin), _relay2Pin(relay2Pin), _seqRetries(0)
{
	pinMode(_relay1Pin, OUTPUT);     //Configura pino do rel� 1
	pinMode(_relay2Pin, OUTPUT);     //Configura pino do rel� 2
}

void InternetMonitor::begin(IPAddress& targetAddress)
{
	_targetAddress = targetAddress;
	_seqRetries = 0;

	this->modemPowerOn();
}

void InternetMonitor::check(void)
{
	ICMPPing ping(pingSocket, (uint16_t)random(0, 255));
	ICMPEchoReply echoReply = ping(_targetAddress, 4);

	this->printEchoReply(echoReply);

	if (echoReply.status == SUCCESS)
	{
		_seqRetries = 0;      			//Zera contador
		_delayBlink.seconds(10);		//Espera 10 segundos (manda um ping a cada 10 segundos, sempre que receber a resposta)
	}
	else
	{
		if (++_seqRetries < 120){		//Tenta at� 120 vezes (equivalente a 2 minutos, 1 tentativa por segundo)
			_delayBlink.seconds(1);     //N�o houve resposta do ping, espera 1 segundo e tenta novamente depois

		}else{							//Excedeu o tempo m�ximo de espera
			this->modemReset();			//Reseta o modem
			_seqRetries = 0; 			//Zera contador
		}
	}

}


inline void InternetMonitor::modemPowerOn(void){
	digitalWrite(_relay1Pin, LOW);
	digitalWrite(_relay2Pin, LOW);
}

inline void InternetMonitor::modemPowerOff(void){
	digitalWrite(_relay1Pin, HIGH);
	digitalWrite(_relay2Pin, HIGH);
}

void InternetMonitor::modemReset(void){

	//Excedeu a quantidade m�xima de timeouts do ping
	Serial.println("Excedeu a quantidade m�xima de timeouts do ping");

	Serial.println("Desligando modem...");
	this->modemPowerOff();
	_delayBlink.seconds(5);		// Espera 5 segundos

	Serial.println("Ligando modem...");
	this->modemPowerOn();

	Serial.println("Esperando dois minutos, at� que o modem se reconecte a internet...");
	_delayBlink.minutes(2);		// Espera 2 minutos
}


void InternetMonitor::printEchoReply(ICMPEchoReply& echoReply){

	if(echoReply.status == SUCCESS){
		snprintf(_print_buffer, sizeof(_print_buffer), "Reply[%d] from: %d.%d.%d.%d: bytes=%d time=%ldms TTL=%d",
				echoReply.data.seq,
				echoReply.addr[0],
				echoReply.addr[1],
				echoReply.addr[2],
				echoReply.addr[3],
				REQ_DATASIZE,
				millis() - echoReply.data.time,
				echoReply.ttl);
	}else{
		snprintf(_print_buffer, sizeof(_print_buffer), "Echo request failed from %d.%d.%d.%d: %s, retries count: %d",
				_targetAddress[0],
				_targetAddress[1],
				_targetAddress[2],
				_targetAddress[3],
				statusStr(echoReply.status),
				_seqRetries);
	}

	Serial.println(_print_buffer);
}

